from  rest_framework  import  serializers

from .models   import  BookInfo


class   BookModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookInfo
        fields = '__all__'