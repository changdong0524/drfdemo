from  rest_framework.viewsets  import ModelViewSet
from .models import   BookInfo
from  .sersizes import BookModelSerializer
from django.shortcuts import render

# Create your views here.


class  StudentViewSet(ModelViewSet):
    queryset =  BookInfo.objects.all()
    serializer_class = BookModelSerializer
