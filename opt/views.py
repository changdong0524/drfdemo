from django.shortcuts import render
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly, AllowAny

# Create your views here.
'''
### 提供的权限

-   AllowAny 允许所有用户
-   IsAuthenticated 仅通过登录认证的用户
-   IsAdminUser 仅管理员用户
-   IsAuthenticatedOrReadOnly 已经登陆认证的用户可以对数据进行增删改操作，没有登陆认证的只能查看数据。

默认/admin中的职员状态为true的时候会认为是管理员

'''


class ExampleAPIView(ViewSet):
    permission_classes = [IsAuthenticated,IsAdminUser]
    #permission_classes = [AllowAny]

    # 认证组件的时候使用
    # http://127.0.0.1:8888/opt/test/auth/
    @action(methods=['get', 'post'], detail=False, url_path='mall')
    def auth(self, request):
        return Response('auth....')

    # http://127.0.0.1:8888/opt/test/login/
    @action(methods=['get', 'post'], detail=False, url_path='login')
    def login(self, request):
        return Response('login....')


from rest_framework.permissions import BasePermission


# 自定义权限 继承父类权限
class ConstumerPermission(BasePermission):

    def has_permission(self, request, view):
        if request.user and request.user.username == 'dong':
            return True


# 自定义权限哦
class Example2APIView(ViewSet):
    permission_classes = [ConstumerPermission]

    # 认证组件的时候使用
    # http://127.0.0.1:8888/opt/test2/auth/
    @action(methods=['get', 'post'], detail=False, url_path="auth")
    def auth(self, request):
        return Response('ConstumerPermission....')


# 限流
class Example3APIView(ViewSet):

    @action(methods=['get'], detail=False, url_path="water")
    def auth(self, request):
        return Response('测试限流....')


# 局部限流
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle, ScopedRateThrottle


class Example4APIView(ViewSet):
    # throttle_classes = [UserRateThrottle,AnonRateThrottle]
    throttle_classes = [UserRateThrottle, AnonRateThrottle]
    throttle_scope = 'python30'  # 自定义配置限制时间

    @action(methods=['get'], detail=False, url_path="hot")
    def auth(self, request):
        return Response('测试局部限流....')


from  rest_framework.viewsets import  GenericViewSet
from  rest_framework.mixins import   ListModelMixin
from  students.models  import  Student
from  students.sersizes  import StudentModelSerializers

class Example5APIView(GenericViewSet,ListModelMixin):
    # 重写内置queryset
    queryset = Student.objects.all()
    serializer_class = StudentModelSerializers
    filter_fields = ('sex','age')


'''
过滤和排序组件是重叠的，
http://127.0.0.1:8888/opt/test6/?age=55&ordering=-id

'''
from  rest_framework.filters import OrderingFilter  #排序
from  django_filters.rest_framework import  DjangoFilterBackend  #过滤类


class Example6APIView(GenericViewSet,ListModelMixin):
    # 重写内置queryset
    queryset = Student.objects.all()
    serializer_class = StudentModelSerializers
    #排序类    过滤类 重叠的 必须都加上才可以同时支持过滤和查询
    filter_backends = [OrderingFilter,DjangoFilterBackend]

    #进行ID倒序排序
    # http://127.0.0.1:8888/opt/test6/?ordering=-id
    #ordering=-id 负数为倒序  正数为正序
    ordering_fields=['id','age']   #排序字段
    filter_fields = ('sex', 'age')  #过滤字段



from  rest_framework.pagination import   LimitOffsetPagination

class  MyPage(LimitOffsetPagination):
    # default_limit = 3  #默认每页条数
    # limit_query_param = 'limit'  #支持url的查询分页为5条
    # max_limit =  5
    # offset_query_param = 'offset'

    # 默认每一页显示的数据量
    page_size = 2
    # 允许客户端通过get参数来控制每一页的数据量
    page_size_query_param = "size"
    max_page_size = 10
    # 自定义页码的参数名
    page_query_param = "p"


from  rest_framework.viewsets import  ViewSet


class pageAPIView(GenericViewSet,ListModelMixin):
    # 重写内置queryset
    queryset = Student.objects.all()
    serializer_class = StudentModelSerializers
    pagination_class = MyPage


#测试数学计算异常
class errorAPIView(ViewSet):


    @action(methods=['get'],detail=False,url_path='error')
    def  list(self,request):
        10 / 0
        return  Response('测试异常。。。。')








