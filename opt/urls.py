from django.urls import path
from . import views

from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('test5/', views.Example5APIView.as_view({'get':'list'})),
    path('error/',views.errorAPIView.as_view({'get':'list'})),

path('docs/', include_docs_urls(title='站点页面标题'))

]


from  rest_framework.routers import   DefaultRouter

router  = DefaultRouter()
#
router.register('test',views.ExampleAPIView,basename='auth')

router.register('test2',views.Example2APIView,basename='auth2')

router.register('test3',views.Example3APIView,basename='water')

router.register('test4',views.Example4APIView,basename='hot')

router.register('test6',views.Example6APIView,basename='order')

router.register('page',views.pageAPIView,basename='page')

urlpatterns += router.urls