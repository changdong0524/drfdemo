from django.db import models


# Create your models here.

class Student(models.Model):

    name = models.CharField(null=False, max_length=32, verbose_name='姓名',help_text='姓名')
    sex = models.BooleanField(default=True, verbose_name='性别',help_text='性别')
    age = models.IntegerField(verbose_name='年龄',help_text='年龄')
    class_num = models.CharField(max_length=5, verbose_name='班级编号',help_text='班级编号')
    description = models.TextField(null=True,blank=True,max_length=100, verbose_name='个性签名',help_text='个性签名')
    #date = models.DateTimeField(auto_now=True)


    class Meta:
        db_table = 'tb_student'
        verbose_name = '学生'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    #模型中如果希望新增自定义属性字段
    @property
    def born(self):
        #return '2021-08-25'
        from  datetime import  datetime
        now = datetime.now().date().year - self.age
        return  now
