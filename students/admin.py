from django.contrib import admin
from .models import Student


# Register your models here.
class StudentModelAdmin(admin.ModelAdmin):
    """学生模型管理类"""
    # date_hierarchy = 'my_born'  # 按指定时间字段的不同值来进行选项排列
    list_display = ['id', "name", "sex", "age", "class_num", "born", 'my_born']  # 设置列表页的展示字段
    ordering = ['-id']  # 设置默认排序字段,字段前面加上-号表示倒叙排列
    actions_on_bottom = True  # 下方控制栏是否显示,默认False表示隐藏
    actions_on_top = True  # 上方控制栏是否显示,默认False表示隐藏
    list_filter = ["class_num"]  # 过滤器,按指定字段的不同值来进行展示
    search_fields = ["name"]  # 搜索内容

    def my_born(self, obj):
        return str(obj.born).split(" ")[0]

    my_born.short_description = "出生日期"  # 自定义字段的描述信息
    my_born.admin_order_field = "born"  # 自定义字段点击时使用哪个字段作为排序条件

    # 详情页配置项  编辑和添加
    # fields = ('name','age','class_num')   #只显示列出的字段
    # readonly_fields = ['name']  #只读 不允许修改

    # 字段集,fieldsets和fields只能使用其中之一
    fieldsets = (
        ("必填项", {
            'fields': ('name', 'age', 'sex')
        }),
        ('可选项', {
            'classes': ('collapse',),  # 折叠样式
            'fields': ('class_num', 'description'),
        }),
    )

    def delete_model(self, request, obj):
        """当站点删除当前模型时执行的钩子方法"""
        print("有人删除了模型信息[添加/修改]")

        # raise Exception("无法删除") # 阻止删除
        return super().delete_model(request, obj)  # 继续删除

    def save_model(self, request, obj, form, change):
        """
        当站点保存当前模型时
        """
        print("有人修改了模型信息[添加/修改]")
        # 区分添加和修改? obj是否有id
        print(obj.id)
        return super().save_model(request, obj, form, change)


admin.site.register(Student, StudentModelAdmin)
