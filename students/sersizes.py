# 序列化器

from rest_framework import serializers
from .models import Student


class StudentModelSerializers(serializers.ModelSerializer):
    # 学生信息序列化器

    # 1字段声明

    # 2模型序列化器相关申明
    class Meta:
        model = Student
        fields = '__all__'
        # fields=['id','name','age']
        extra_kwargs = {
            'age': {
                'required': True,
                'help_text': '年龄2'
            }
        }


# 3验证代码  反序列化
# 4操作数据  ---反序列化

class StudentModel2Serializers(serializers.ModelSerializer):
    # 学生信息序列化器
    # 1字段声明
    # 2模型序列化器相关申明
    class Meta:
        model = Student
        fields = ['id', 'name', 'age']
# 3验证代码  反序列化


# 4操作数据  ---反序列化
