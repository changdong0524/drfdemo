from  rest_framework.viewsets  import ModelViewSet
from .models import   Student
from  .sersizes import StudentModelSerializers
from django.shortcuts import render

# Create your views here.


class  StudentViewSet(ModelViewSet):
    queryset =  Student.objects.all()
    serializer_class = StudentModelSerializers





