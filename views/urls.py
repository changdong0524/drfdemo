from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path('^students/', views.StudentAPIViewSet.as_view({"get": "list", "post": "post", })),  # 查询多个 提交
    re_path('^students2/(?P<pk>\d+)$',views.StudentAPIViewSet.as_view({'get': 'get', 'put': 'put', 'delete': 'delete'})),  # 查询单个、修改、删除

    re_path('^students3/', views.Student2APIViewSet.as_view({"get": "get", "post": "post", })),  # 查询多个 提交
    re_path('^students4/(?P<pk>\d+)$',views.Student2APIViewSet.as_view({'get': 'get_one', 'put': 'put', 'delete': 'delete'})),  # 查询单个、修改、删除

    re_path('^students5/', views.Student3APIViewSet.as_view({"get": "list", "post": "create", })),  # 查询多个 提交
    re_path('^students6/(?P<pk>\d+)$',views.Student3APIViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),  # 查询单个、修改、删除
    # 使用最简单的测试
    re_path('^students7/', views.Student4APIViewSet.as_view({"get": "list", "post": "create", })),  # 查询多个 提交
    re_path('^students8/(?P<pk>\d+)$',views.Student4APIViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),  # 查询单个、修改、删除

#根据请求方法不同 显示不同的字段
    re_path('^students9/', views.Student5APIViewSet.as_view({"get": "list", "post": "create", })),  # 查询多个 提交
    re_path('^students10/(?P<pk>\d+)$',views.Student5APIViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),  # 查询单个、修改、删除


]

#使用路由类给视图集生成路由
from  rest_framework.routers import SimpleRouter,DefaultRouter

router = DefaultRouter()
#路由访问前缀  视图集类 basename路由别名
router.register("dongdong",views.Student5APIViewSet)
#http://127.0.0.1:8888/views/dongdong/
print(router.urls)
#把路由类生成的路由信息和上面进行合并
urlpatterns += router.urls