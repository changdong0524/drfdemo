from django.shortcuts import render
from students.sersizes import StudentModelSerializers,StudentModel2Serializers
from  django.http  import   HttpResponse
from students.models import Student
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework import status
from rest_framework.mixins import ListModelMixin


# Create your views here.
class StudentAPIViewSet(ViewSet):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    def list(self, request):
        # 获取多条数据
        serializer = self.serializer_class(instance=self.queryset.all(), many=True)  # 法1
        # serializer = self.serializer_class(instance=self.queryset.all(), many=True)  #法2
        return Response(serializer.data)

    # 新增数据
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def get(self, request, pk):
        # 获取单条数据
        data = self.queryset.get(pk=pk)  # 法1
        serializer = self.serializer_class(instance=data)
        return Response(serializer.data)

    # 修改
    def put(self, request, pk):
        # 获取单条数据
        data = self.queryset.get(pk=pk)
        # 多了修改的过程
        serializer = self.serializer_class(instance=data, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    # 删除
    def delete(self, request, pk):
        self.queryset.get(pk=pk).delete()
        return Response(status=status.HTTP_200_OK)


from rest_framework.viewsets import GenericViewSet


class Student2APIViewSet(GenericViewSet):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    def get(self, request):
        # 获取多条数据
        serializer = self.serializer_class(instance=self.queryset.all(), many=True)  # 法1
        # serializer = self.serializer_class(instance=self.queryset.all(), many=True)  #法2
        return Response(serializer.data)

    # 新增数据
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def get_one(self, request, pk):
        # 获取单条数据
        data = self.queryset.get(pk=pk)  # 法1
        serializer = self.serializer_class(instance=data)
        return Response(serializer.data)

    # 修改
    def put(self, request, pk):
        # 获取单条数据
        data = self.queryset.get(pk=pk)
        # 多了修改的过程
        serializer = self.serializer_class(instance=data, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    # 删除
    def delete(self, request, pk):
        self.queryset.get(pk=pk).delete()
        return Response(status=status.HTTP_200_OK)


###因为框架已经帮我们封装好了，所以可以直接用

from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin, \
    DestroyModelMixin


class Student3APIViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin,
                         DestroyModelMixin):
    serializer_class = StudentModelSerializers
    queryset = Student.objects.all()


# ModelViewSet 就是集成了上面的所有方法

# ReadOnlyModelViewSet
class Student4APIViewSet(ModelViewSet):
    serializer_class = StudentModelSerializers
    queryset = Student.objects.all()


from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin, DestroyModelMixin


from  rest_framework.decorators import  action

#根据请求方法不同 显示不同的字段
class Student5APIViewSet(ReadOnlyModelViewSet, CreateModelMixin, UpdateModelMixin, DestroyModelMixin):
    serializer_class = StudentModelSerializers
    queryset = Student.objects.all()


    def  get_serializer_class(self):
        print(self.action) #本次请求的视图方法名称
        if  self.action.lower() == 'list':   #列表就只显示3个字段
            return StudentModel2Serializers
        else:  #其他情况 显示全部字段
            return StudentModelSerializers

    '''
    系统默认的5个接口地址，已经给我们封装了，可以直接调用
    但是自己定义的接口地址，需要进行配置，使用decorators 
    methods 设置哪些请求方法能访问到该视图方法
    detail 设置生成路由时候，是否附带ID到地址中
    url_path 设置访问当前方法的子路由  如果不配置 则默认是当前的方法名
    '''
    @action(methods=['get','post'],detail=False,url_path='test')
    def  test_api(self,request):   #http://127.0.0.1:8888/views/dongdong/test/
        return  Response('test......')





