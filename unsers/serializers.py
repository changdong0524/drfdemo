from rest_framework import serializers

from students.models import Student


# 字段验证函数【只能用于一个字段验证函数】
def check_class_num(data):
    if data == '404':
        raise serializers.ValidationError('对不起没有404这个班级名称')
    return data


# 注意这里继承Serializer,不需要meta
class StudentSerializer(serializers.Serializer):
    # 需要进行转换的字段
    # 反序列化的时候  id 不存在     序列化的时候才存在
    # 当readonly设置为true的时候   在序列化存在   反序列化被忽略
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=20, required=True)
    age = serializers.IntegerField(max_value=100, required=True)
    sex = serializers.BooleanField(allow_null=True, default=1)
    class_num = serializers.CharField(required=True, validators=[check_class_num])
    description = serializers.CharField(allow_null=True, allow_blank=True)

    # validate()  验证所有字段
    # validate_字段  验证单个字段
    def validate_name(self, attr):
        print(attr)
        if attr in '东东':
            raise serializers.ValidationError('不能输入非法关键字。。。')
        # 必须返回 不然数据就会丢失
        return attr

    def validate(self, attrs):
        print(attrs)
        if attrs.get('age') > 60 and attrs.get('sex') == 1:
            raise serializers.ValidationError('对不起你的年纪太大了')

        # 必须返回 不然数据就会丢失
        return attrs

    # 添加数据
    def create(self, validated_data):
        # 自动修正格式
        ret = Student.objects.create(**validated_data)
        # 返回结果
        return ret

    # 更新数据
    def update(self, instance, validated_data):
        instance.name = validated_data.get('name')
        instance.age = validated_data.get('age')
        instance.sex = validated_data.get('sex')
        instance.class_num = validated_data.get('class_num')
        instance.description = validated_data.get('description')

        '''
        #同理  同上  法2
        for  key,value  in   validated_data.items():
            setattr(instance,key,value)
        
        '''


        instance.save()
        return instance


class StudentModelSerializer(serializers.ModelSerializer):
    #默认字段
    token   = serializers.CharField(read_only=True,default='abcdef123456')
    class Meta:
        model = Student
        fields = ['id','name','age','token']
        read_only_fields = ['id']
        #额外字段
        extra_kwargs={
            "token":{
                "read_only":True
            },
            "age":{
                "min_value":1,
                "max_value":80
            }
        }