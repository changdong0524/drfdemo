from django.shortcuts import render,HttpResponse
from  django.http.response import  JsonResponse
from  django.views   import  View
from .serializers import  StudentSerializer
from  students.models  import  Student


# Create your views here.

class StudentView(View):
    def  get1(self,request):
        #实例化序列器创建序列化对象
        '''
        :参数
            instance 模型或者模型列表
            data  进行反序列化阶段中使用的数据，数据来自客户端的提交
            context 当需要从视图中转发数据到序列化器时，可以使用
            many 当instance参数为模型列表时候，则many的值必须是true

        :param request:
        :return:
        '''
        student_list = Student.objects.all()
        print(student_list)
        serializer = StudentSerializer(instance=student_list,many=True)
        print(serializer.data)
        return  JsonResponse(serializer.data,safe=False)


    def  get(self,request):
        student_one =Student.objects.get(pk=3)
        print(student_one)
        serializer = StudentSerializer(instance=student_one)
        print(serializer.data)
        return JsonResponse(serializer.data,safe=False)