from rest_framework import serializers

from  students.models import Student

# 注意这里继承Serializer,不需要meta
class StudentSerializer(serializers.Serializer):
    # 需要进行转换的字段
    name = serializers.CharField()
    age  = serializers.IntegerField()
    sex  = serializers.BooleanField()

    # class Meta:
    #     model = Student
    #     fields = '__all__'

    def  update(self, instance, validated_data):

        instance.name = validated_data.get('name')
        instance.age = validated_data.get('age')
        instance.sex = validated_data.get('sex')
        instance.class_num = validated_data.get('class_num')
        instance.description = validated_data.get('description')
        instance.save()
        return  instance


#学生模型序列化器
class StudentModelSerializer(serializers.ModelSerializer):
    #声明要转换的字段

    #模型声明
    class Meta:
        model = Student
        fields=['id','name','age']
    #验证方法

    #数据库操作方法


