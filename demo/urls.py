from django.urls import path, re_path
from . import views

urlpatterns = [
    path('students1/', views.Student1APIView.as_view()),
    path('students2/', views.Student2APIView.as_view()),  # http://127.0.0.1:8888/demo/students2/
    re_path('^students3/(?P<pk>\d+)$', views.Student3APIView.as_view()),
]
