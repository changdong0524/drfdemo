from django.shortcuts import render
from rest_framework.views import APIView
from django.views import View
from django.http.response import HttpResponse



'''
students/  get  获取多个个数据
students/  post  提交数据
students/<PK>   put  修改单个数据
students/<PK>   delete  删除单个数据




students/<PK>   get  获取单个数据
students/<PK>   put  修改单个数据
students/<PK>   delete  删除单个数据

'''

# Create your views here.

class Student1APIView(APIView):
    def get(self, request):
        print(request)
        return HttpResponse('ok')


from students.models import Student
from students.sersizes import StudentModelSerializers
from rest_framework.response import Response
from  rest_framework  import  status

#获取多个数据的类
class Student2APIView(APIView):
    def get(self, request):
        # 获取多条数据
        student_list = Student.objects.all()
        serializer = StudentModelSerializers(instance=student_list, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer  = StudentModelSerializers(data = request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()
        return Response(serializer.data)


#单独抽离一个类 用作获取单个数据  需要传递ID
class Student3APIView(APIView):
    def get(self, request, pk):
        # 获取单条数据
        student_one = Student.objects.get(pk=pk)
        serializer = StudentModelSerializers(instance=student_one)
        return Response(serializer.data)

    def  put(self,request,pk):
        # 获取单条数据
        student = Student.objects.get(pk=pk)
        #多了修改的过程
        serializer = StudentModelSerializers(instance=student,data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    #删除
    def   delete(self,request,pk):
        Student.objects.get(pk=pk).delete()
        return Response(status=status.HTTP_200_OK)