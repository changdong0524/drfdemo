from django.urls import path
from . import views
urlpatterns = [
    path('students/',views.StudentAPIView.as_view()),
    path('default/', views.StudentView.as_view()),  #http://127.0.0.1:8888/req/default/

]
