from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response


# Create your views here.


class StudentAPIView(APIView):
    def get(self, request):
        # print(request)  #<rest_framework.request.Request: GET '/req/students/'>
        # print(request._request)    #<WSGIRequest: GET '/req/students/'>
        print(request.query_params)  # <QueryDict: {'id': ['123']}>
        '''
         # http://127.0.0.1:8888/req/students/?id=123&age=18&like=ball&like=video
         get 获取单个值
         getlist  获取多个值 是一个数组/列表
         
        '''
        age = request.query_params.get('age')
        like = request.query_params.getlist('like')
        print(age, like)
        return Response('ok')

    def post(self, request):
        '''
            post 提交 raw  json
            {'id': 111, 'name': 'dong', 'like': 'ball'}
            post提交 form data格式
            <QueryDict: {'type': ['1'], 'name': ['yue'], 'image': [<InMemoryUploadedFile: 2.jpg (image/jpeg)>]}>
        :param request:
        :return:
        '''
        print(request.data)
        return Response('提交成功')


from django.views import View
from  rest_framework import  status
class StudentView(APIView):
    def get(self, request):
        response  = Response('OKK',status=status.HTTP_200_OK)
        #返回 设置cookie
        response.set_cookie('username','dongdong')
        #返回响应头
        response['company']="old boy"
        return response

