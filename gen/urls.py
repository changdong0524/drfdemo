from django.urls import path, re_path
from . import views

urlpatterns = [
    path('students1/', views.Student1APIView.as_view()),
    re_path('^students2/(?P<pk>\d+)$', views.Student2APIView.as_view()),  # http://127.0.0.1:8888/gen/students2/3

    # 视图扩展类1
    path('students3/', views.Student3APIView.as_view()),  # 查询多个列表和新增接口
    re_path('^students4/(?P<pk>\d+)$', views.Student4APIView.as_view()),  # 查询单个、修改、删除

    # 测试封装好的api 视图扩展类2
    path('students5/', views.Student5APIView.as_view()),  # 查询多个列表和新增接口
    re_path('^students6/(?P<pk>\d+)$', views.Student6APIView.as_view()),  # 查询单个、修改、删除

    re_path('students7/(?P<pk>\d+)$', views.Student7APIView.as_view()), # 查询单个、修改、
    re_path('students8/(?P<pk>\d+)$', views.Student8APIView.as_view()),  # 查询单个、修改、删除
]
