from django.http.response import HttpResponse
from rest_framework.generics import GenericAPIView
from students.models import Student
from students.sersizes import StudentModelSerializers, StudentModel2Serializers
from rest_framework.response import Response
from rest_framework import status

'''
students/  get  获取多个个数据
students/  post  提交数据
students/<PK>   put  修改单个数据
students/<PK>   delete  删除单个数据

students/<PK>   get  获取单个数据
students/<PK>   put  修改单个数据
students/<PK>   delete  删除单个数据

'''


class Student1APIView(GenericAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    # 自定义  1个类下面可以调用不同的序列化器
    # 应用  比如可以根据请求方式 显示不同的序列化器 同时显示不同的字段 或者展示/修改不同的字段
    def get_serializer_class(self):
        if  self.request  and self.request.method.lower() == 'get':
            return StudentModel2Serializers
        else:
            return StudentModelSerializers

    def get(self, request):
        # 获取多条数据
        serializer = self.get_serializer(instance=self.queryset.all(), many=True)  # 法1
        # serializer = self.serializer_class(instance=self.queryset.all(), many=True)  #法2
        return Response(serializer.data)

    # 新增数据
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


# 单独抽离一个类 用作获取单个数据  需要传递ID
class Student2APIView(GenericAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    def get(self, request, pk):
        # 获取单条数据
        # data = self.queryset.get(pk=pk)  #法1
        data = self.get_object()  # 法2
        serializer = self.serializer_class(instance=data)
        return Response(serializer.data)

    # 修改
    def put(self, request, pk):
        # 获取单条数据
        data = self.queryset.get(pk=pk)
        # 多了修改的过程
        serializer = self.serializer_class(instance=data, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    # 删除
    def delete(self, request, pk):
        self.queryset.get(pk=pk).delete()
        return Response(status=status.HTTP_200_OK)


'''
视图扩展类1
1）ListModelMixin 获取多条数据
2）CreateModelMixin  新增一条数据
3）RetrieveModelMixin  获取一条数据
4）UpdateModelMixin  更新一条数据
5）DestroyModelMixin  删除一条数据


'''
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, \
    DestroyModelMixin


class Student3APIView(GenericAPIView, ListModelMixin, CreateModelMixin):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    # ListModelMixin
    def get(self, request):
        # 获取多条数据
        return self.list(request)

    # CreateModelMixin
    def post(self, request):
        # 添加一条数据
        return self.create(request)


class Student4APIView(GenericAPIView, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦

    # RetrieveModelMixin
    def get(self, request, pk):
        # 获取多条数据
        return self.retrieve(request, pk)

    # UpdateModelMixin
    def put(self, request, pk):
        return self.update(request, pk)

    # DestroyModelMixin
    def delete(self, request, pk):
        return self.destroy(request, pk)


'''
视图扩展类2
由视图扩展类和通用类进行组合，产生新的这些视图子类，让我们可以开发基本的api接口，更加简单那


1）ListAPIView   =            GenericAPIView + ListModelMixin 获取多条数据
2）CreateAPIView  =           GenericAPIView + CreateModelMixin  新增一条数据
3） RetrieveAPIView  =        GenericAPIView + RetrieveModelMixin  获取一条数据
4） UpdateAPIView =           GenericAPIView + UpdateModelMixin  更新一条数据
5） DestroyAPIView =          CreateAPIView  + DestroyModelMixin  删除一条数据


'''

from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView


# 查询多个列表ListAPIView，新增CreateAPIView（不需要pk的ID）
class Student5APIView(ListAPIView, CreateAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦


# 查询单个RetrieveAPIView  修改单个UpdateAPIView  删除单个DestroyAPIView （因为需要pk的ID）
class Student6APIView(RetrieveAPIView, UpdateAPIView, DestroyAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦


from rest_framework.generics import RetrieveUpdateAPIView,DestroyAPIView


# 提供了get   put  patch
# 查询单个RetrieveAPIView  修改单个UpdateAPIView  删除单个DestroyAPIView （因为需要pk的ID）
# RetrieveUpdateAPIView
class Student7APIView(RetrieveUpdateAPIView, DestroyAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦


# 提供了get   put  patch delete
from rest_framework.generics import RetrieveUpdateDestroyAPIView
class Student8APIView(RetrieveUpdateDestroyAPIView):
    serializer_class = StudentModelSerializers
    # 重写内置queryset
    queryset = Student.objects.all()  # 会自动把all删掉哦